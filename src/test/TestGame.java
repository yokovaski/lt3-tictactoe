package test;

import ttt.TicTacToeGame;
import junit.framework.TestCase;

public class TestGame extends TestCase{
	private TicTacToeGame play;
	
	public TestGame(String arg0) {
		super(arg0);
	}
	
	public void testPositionValue(){
		play = new TicTacToeGame();
		play.setComputerPlays();
		assertEquals(TicTacToeGame.UNCLEAR, play.positionValue());
		
		play.playMove(0);
		play.playMove(6);
		play.playMove(1);
		play.playMove(7);
		play.playMove(2);
		assertEquals(TicTacToeGame.COMPUTER_WIN, play.positionValue());
	
		play = new TicTacToeGame();
		play.setHumanPlays();
		play.playMove(0);
		play.playMove(6);
		play.playMove(1);
		play.playMove(7);
		play.playMove(2);
		assertEquals(TicTacToeGame.HUMAN_WIN, play.positionValue());
		
		play = new TicTacToeGame();
		play.playMove(0);
		play.playMove(1);
		play.playMove(2);
		play.playMove(5);
		play.playMove(3);
		play.playMove(6);
		play.playMove(7);
		play.playMove(8);
		play.playMove(4);
		assertEquals(TicTacToeGame.DRAW, play.positionValue());
	}
	
	public void testIsAWin(){
		play = new TicTacToeGame();
		play.setHumanPlays();
		play.playMove(0);
		play.playMove(8);
		play.playMove(1);
		play.playMove(7);
		play.playMove(2);
		assertEquals(false, play.isAWin(TicTacToeGame.COMPUTER));
		assertEquals(true, play.isAWin(TicTacToeGame.HUMAN));
		
		play = new TicTacToeGame();
		assertEquals(false, play.isAWin(TicTacToeGame.HUMAN));
		assertEquals(true, play.isAWin(TicTacToeGame.COMPUTER));
		
		play.setComputerPlays();
		play.playMove(0);
		play.playMove(6);
		play.playMove(1);
		play.playMove(7);
		play.playMove(2);
		assertEquals(false, play.isAWin(TicTacToeGame.HUMAN));
		assertEquals(true, play.isAWin(TicTacToeGame.COMPUTER));
	}
	
	public void chooseMove(){
		play = new TicTacToeGame();
		play.setHumanPlays();
		play.playMove(6);
		play.playMove(1);
		play.playMove(8);
		assertEquals(7, play.chooseMove());
		
		play = new TicTacToeGame();
		play.setComputerPlays();
		play.playMove(0);
		play.playMove(2);
		play.playMove(8);
		play.playMove(7);
		assertEquals(4, play.chooseMove());
		
		play = new TicTacToeGame();
		play.setComputerPlays();
		play.playMove(0);
		play.playMove(8);
		play.playMove(1);
		play.playMove(6);
		assertEquals(2, play.chooseMove());
	}
}